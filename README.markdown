###Projeto Open Source para desenvolvedores.
Com intuito de melhorar a criação de um projeto simples, onde irá trabalhar com <a href="gulpjs.com" target="_blank">Gulp Automatizador</a>, decidir desenvolver um gerador de código, pronto pra trabalhar com alguns plugins já instalados, configurados e com assets também já criados.

Desenvolvi com Ruby, e pitadas de Javascript.

###Necessario para funcionamento.
<ol>
  <li><a href="https://rvm.io">Ruby</a></li>
  <li><a href="http://nodejs.org/download">NodeJS (npm)</a></li>
  <li>Extensão LiveReload Chrome</li>
</ol>

###Dependências já instaladas
<ol>
  <li>Gulp</li>
	<li>Ruby-Sass</li>
	<li>Autoprefixer</li>
	<li>LiveReload</li>
	<li>Minify-css</li>
	<li>Tiny-lr</li>
</ol>

### CRÉDITOS E CONTATOS
Thadeu Esteves Jr.<br>
**@desenvolvedor_web || =  "Ruby on Rails and PHP Programmer"<br>**
celular.: 063 9275-6226<br>
whatsapp.: 063 9275-6226<br>
e-mail.: tadeu.palmas@hotmail.com / tadeuu@gmail.com<br>
skype.: tadeu.esteves<br>
site.: thadeuesteves.com.br<br>
blog.: tadeuesteves.wordpress.com<br>
github.: https://github.com/thadeu<br>
